import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from "ngx-spinner";
import Swal from 'sweetalert2'

@Component({
  selector: 'app-buybook',
  templateUrl: './buybook.component.html',
  styleUrls: ['./buybook.component.css']
}) 
export class BuybookComponent implements OnInit {
  bookdata: any = [];
  buyUserForm: FormGroup;
  submitted = false;
  paytm = [];
  paymentMode:any;
  apiEndPoint = environment.apiUrl;
  imageUrl = environment.imageUrl;
  constructor(private formBuilder: FormBuilder, private http: HttpClient, private router: Router,private spinner: NgxSpinnerService) { }

  ngOnInit() {
    let useridentify = localStorage.getItem('checkuser');
    let paymentMoney = localStorage.getItem('xyz');
    let paymentType = localStorage.getItem('paymentMode');

    if (useridentify === '1' && paymentMoney === '6') {
    
      this.buyUserForm = this.formBuilder.group({
        name: ['', [Validators.required]],
        email: ['', [Validators.required]],
        mbnumber: [, [Validators.required, Validators.pattern('[6-9]\\d{9}')]],
        deliveryaddress: ['', [Validators.required]],
      }
      )
      let postbookid = localStorage.getItem('postBookId');
      if(paymentType === '9'){
        this.paymentMode = "COD"
      }else{
        this.paymentMode = "Online"
      }
      this.spinner.show();
      this.http.get(this.apiEndPoint +'getBuyBookDetail/' + postbookid).subscribe((response: any) => {
        this.spinner.hide();
        this.bookdata = response.doc;
        localStorage.setItem('sellingprice',response.doc.sellingprice);
        localStorage.setItem('bookname',response.doc.bookname);
        localStorage.setItem('authorname',response.doc.authorname);
        localStorage.setItem('actualprice',response.doc.actualprice);
        localStorage.setItem('isdnnumber',response.doc.isdnnumber);
        localStorage.setItem('category',response.doc.category);
        localStorage.setItem('department',response.doc.department);
        localStorage.setItem('edition',response.doc.edition);
      })
    }
    else {
      Swal.fire("please first login to buy book");
      this.router.navigateByUrl('login');
    }
  }
  get f() { return this.buyUserForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.buyUserForm.valid) {
      let postbookid = localStorage.getItem('postBookId');
      let sellingprice = localStorage.getItem('sellingprice');
      let bookname = localStorage.getItem('bookname');
      let authorname = localStorage.getItem('authorname');
      let actualprice = localStorage.getItem('actualprice');
      let isdnnumber = localStorage.getItem('isdnnumber');
      let category = localStorage.getItem('category');
      let department = localStorage.getItem('department');
      let edition = localStorage.getItem('edition');
      let _userid = localStorage.getItem('userid');
      let gstTax = localStorage.getItem('gstTax')
      this.buyUserForm.value.postbookid = postbookid;
      this.buyUserForm.value.sellingprice = sellingprice;
      this.buyUserForm.value.bookname = bookname;
      this.buyUserForm.value.authorname = authorname;
      this.buyUserForm.value.actualprice = actualprice;
      this.buyUserForm.value.isdnnumber = isdnnumber;
      this.buyUserForm.value.category = category;
      this.buyUserForm.value.department = department;
      this.buyUserForm.value.edition = edition;
      this.buyUserForm.value._userid = _userid;
      this.buyUserForm.value.gstTax = gstTax;
      this.buyUserForm.value.paymentMode = this.paymentMode;
      this.buyUserForm.value.deliveryCharge = 'Free';
      this.buyUserForm.value.totalPrice = localStorage.getItem('totalPayment');
      this.spinner.show();
      this.http.post(this.apiEndPoint +'sendBuyerDetail',this.buyUserForm.value).subscribe((response) => {
        this.spinner.hide();
        Swal.fire('You Buy the book Successfully');
        this.router.navigateByUrl('newhome');
      })
    }
    // else {
    //   alert('Your from is not valid');
    // }
  }


}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from "ngx-spinner";
import Swal from 'sweetalert2'

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {
  loginForm: FormGroup;
  data1: Object;
  id: any;
  submitted = false;
  apiEndPoint = environment.apiUrl;
  constructor(private router: Router, private formBuilder: FormBuilder, private http: HttpClient,private spinner: NgxSpinnerService) { }

  ngOnInit() {
    localStorage.clear();
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });
  }
  get f() { return this.loginForm.controls; }
  onSubmit() {
    this.submitted = true; 
    if (this.loginForm.valid) {
      this.spinner.show();
      this.http.post(this.apiEndPoint +'forgetPassword', this.loginForm.value)
        .subscribe((response) => {
          this.spinner.hide();
          if (response['status'] === true) {
           Swal.fire('Your password has been send to your email address')
            this.router.navigateByUrl('login');
          }
          else {
            console.log("========",response['message']);
            Swal.fire(response['message']);
            this.ngOnInit();
          } 
        }) 
    }
    // else {
    //   alert('invalid Entries');
    // }
  }
}

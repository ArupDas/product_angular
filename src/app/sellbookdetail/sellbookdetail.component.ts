import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from "ngx-spinner";
require('aws-sdk/dist/aws-sdk');

@Component({
  selector: 'app-sellbookdetail',
  templateUrl: './sellbookdetail.component.html',
  styleUrls: ['./sellbookdetail.component.css']
})
export class SellbookdetailComponent implements OnInit {
  data:any;
  tempdata:any=[];
  requests: any = [];
  public searchbox = "";
  request: any = {};
  pager:any  = {};
  pageOfItems:any = [];
  x:any = [];
  nextPage : number;
  pagenumbers:any;
  apiEndPoint = environment.apiUrl;
  constructor(private http: HttpClient, private router: Router,private spinner: NgxSpinnerService) { }

  ngOnInit() {
    let useridentify = localStorage.getItem('checkuser');
    if(useridentify==='1')
    { 
      this.loadPage(1,this.searchbox);
  }
  else{
    this.router.navigateByUrl('');
  }

  
  }
  loadPage(pagenumber,searchbox){
    this.pagenumbers = pagenumber;
    let userid = localStorage.getItem('userid');
    if(searchbox=="")
    {
      this.spinner.show();
      this.http.get(this.apiEndPoint + 'sellBookDetails/'+userid+'/'+this.pagenumbers).subscribe((x:any)=>{
        this.spinner.hide();
        this.pager = x.pager;
           this.pageOfItems = x.pageOfItems;
           this.nextPage = parseInt(this.pager.currentPage)+1;
      })
    }
    else{
      this.spinner.show();
      this.http.get(this.apiEndPoint + 'sellbookdetailsearchitem/'+searchbox+'/'+this.pagenumbers+'/'+userid).subscribe(
        (x:any)=>{
          this.spinner.hide();
          this.pager = x.pager;
          this.pageOfItems = x.pageOfItems;
          this.nextPage = parseInt(this.pager.currentPage)+1;
        }
      ) 
    }
  
  }

  searchItem(searchData)
  {
    let userid = localStorage.getItem('userid');
    let pagenumber1 = this.pagenumbers;
    this.searchbox = searchData;
    if(searchData==""){
      this.loadPage(1,this.searchbox);
    }
    else{
      this.spinner.show();
        this.http.get(this.apiEndPoint + 'sellbookdetailsearchitem/'+searchData+'/'+pagenumber1+'/'+userid).subscribe(
          (x:any)=>{
            this.spinner.hide();
            this.pager = x.pager;
            this.pageOfItems = x.pageOfItems;
            this.nextPage = parseInt(this.pager.currentPage)+1;
          }
        )  
    }
  
  }

  deletebook(value,imageData)
  {
    let AWSService = (<any>window).AWS;
    AWSService.config.accessKeyId = 'AKIAJIKMW36RDZPWRKWQ';
    AWSService.config.secretAccessKey = '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';
    let s3 = new AWSService.S3();
s3.deleteObject({
  Bucket: 'secondhandbookimage',
  Key: imageData
},function (err,data){
  if (data) {
   // console.log("File deleted successfully");
}
else {
   // console.log("Check if you have sufficient permissions : "+err);
}
})
this.deleteBookApiCall(value); 
}

deleteBookApiCall(value){
  this.spinner.show();
  this.http.get(this.apiEndPoint + 'deletebook/'+value).subscribe((response)=>{
    this.spinner.hide();
   this.ngOnInit();
  })
}

  editBookDetail(val){
    this.router.navigate(['/editbook'],{
      queryParams:{bookId:val}
    });
  }

}

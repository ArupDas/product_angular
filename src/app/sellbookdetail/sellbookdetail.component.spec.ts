import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellbookdetailComponent } from './sellbookdetail.component';

describe('SellbookdetailComponent', () => {
  let component: SellbookdetailComponent;
  let fixture: ComponentFixture<SellbookdetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellbookdetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellbookdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

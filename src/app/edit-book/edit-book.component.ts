import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { MustMatch } from '../_helpers/must-match.validator';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from "ngx-spinner";
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html',
  styleUrls: ['./edit-book.component.css']
})
export class EditBookComponent implements OnInit {
  postUserForm: FormGroup;
  submitted = false;
  public imagePath;
  imgURL: any;
  public frontbookimage:any;
  apiEndPoint = environment.apiUrl;
  imageUrl = environment.imageUrl
  ImageFile:any = null;
  bookId:any = null;
  bookImage:any = null;
 

  constructor(private formBuilder: FormBuilder, private route:ActivatedRoute,private http: HttpClient, private router: Router,private spinner: NgxSpinnerService) { }

  ngOnInit() {
    let useridentify = localStorage.getItem('accessToken');
    this.route.queryParams.subscribe(params =>{
      this.bookId = params.bookId;
    })
    if(useridentify)
    { 
     this.formInilization();
     this.setValue();
    }
    else{
      Swal.fire("Please first Login, Then Post a Book");
      this.router.navigateByUrl('login');
    }
  } 
  setValue(){
    let postbookid = this.bookId;
    this.http.get(this.apiEndPoint + 'product/'+postbookid).subscribe((response:any) => {
      let data = response
      this.postUserForm.patchValue({
        bookname:data.productName,
       
      })

    })
  }

  formInilization(){
    this.postUserForm = this.formBuilder.group({
      bookname: ['', [Validators.required]],
     
    }
    )
  }
  get f() { return this.postUserForm.controls; }
   
  onSubmit() {
    this.submitted = true;
    if (this.postUserForm.valid) {
      this.postUserForm.value.postbookid = this.bookId;
      let body = {
        productId:this.bookId,
        productName:this.postUserForm.value.bookname,
        imgUrl:'dummy url'
      }
      // this.spinner.show();
      this.http.put(this.apiEndPoint + 'product', body).subscribe((response) => {
        if(response){
          // this.spinner.hide();
        Swal.fire('You Update Successfully');
       this.router.navigateByUrl('newhome');
      // this.ngOnInit();
        }
        
      },err => {
        // this.spinner.hide();
        console.log("err",err)
        Swal.fire(err.error.message);
      })
    }
  }

  isbnExample()
  {
    Swal.fire({
      title: 'You can find ISBN No. to the back cover of your book or 1st, 2nd page from front side',
      imageUrl: 'assets/images/isbnImage.jpg',
      imageHeight: 200,
      imageWidth:200
      
    })
  }
}

import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from "ngx-spinner";
import Swal from 'sweetalert2'

@Component({
  selector: 'app-newhome',
  templateUrl: './newhome.component.html',
  styleUrls: ['./newhome.component.css']
})
export class NewhomeComponent implements OnInit {
  requests: any = [];
  public searchbox = "";
  public category = ""; 
  public branch = "";
  request: any = {};
  pager:any  = {};
  pageOfItems:any = [];
  x:any = [];
  nextPage : number;
  pagenumbers:any;
  apiEndPoint = environment.apiUrl;
  imageUrl = environment.imageUrl;
  checkRole:any;

  constructor(private http: HttpClient, private router: Router,private spinner: NgxSpinnerService) { }

  ngOnInit() {
    let useridentify = localStorage.getItem('accessToken');
    if(useridentify)
    { 
      let getRoleId = JSON.parse(atob(useridentify.split('.')[1]))
      let permission = getRoleId.permission
      let perms = permission.split(',');
      let obj = {
        create:'',
        update:'',
        fetch:'',
        delete:''
      }
      for(let i = 0; i < perms.length; i++){
        console.log("item",perms[i])
        if(perms[i] == 'create'){
          obj.create  = 'create'
        }
        if(perms[i] == 'update'){
          obj.update  = 'update'
        }
        if(perms[i] == 'fetch'){
          obj.fetch  = 'fetch'
        }
        if(perms[i] == 'delete'){
          obj.delete  = 'delete'
        }
      }

      
      this.checkRole = obj
      console.log("check role",this.checkRole)
    this.loadPage();
  }
  else{
    this.router.navigateByUrl('');
  }
  }

  loadPage(){
      this.http.get(this.apiEndPoint +'product').subscribe(
        (x: any) => {
          console.log("x====",x)
         this.pageOfItems = x;
        },err => {
          // this.spinner.hide();
          console.log("err",err)
          Swal.fire(err.error.message);
        }
      );
  
  }
  

  buyBookDetail(val){
    localStorage.setItem("postBookId",val);
  }

}

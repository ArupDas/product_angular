import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuybookdetailComponent } from './buybookdetail.component';

describe('BuybookdetailComponent', () => {
  let component: BuybookdetailComponent;
  let fixture: ComponentFixture<BuybookdetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuybookdetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuybookdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

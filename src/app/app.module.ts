import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { RegistrationComponent } from './registration/registration.component';
import { HttpClientModule,HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { BookpostComponent } from './bookpost/bookpost.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { NewhomeComponent } from './newhome/newhome.component';
import { LogoutComponent } from './logout/logout.component';
import { AccountComponent } from './account/account.component';
import { BuybookComponent } from './buybook/buybook.component';
import { BuybookdetailComponent } from './buybookdetail/buybookdetail.component';
import { SellbookdetailComponent } from './sellbookdetail/sellbookdetail.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { BuybookpaymentComponent } from './buybookpayment/buybookpayment.component';
import { LottieModule } from 'ngx-lottie';
import player from 'lottie-web';
import { EditBookComponent } from './edit-book/edit-book.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import {NgxImageCompressService} from 'ngx-image-compress';
import { HttpconfigInterceptor } from './auth-interceptor';
 

export function playerFactory() {
  return player;
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RegistrationComponent,
    LoginComponent,
    BookpostComponent,
    HeaderComponent,
    FooterComponent,
    NewhomeComponent,
    LogoutComponent,
    AccountComponent,
    BuybookComponent,
    BuybookdetailComponent,
    SellbookdetailComponent,
    PageNotFoundComponent,
    BuybookpaymentComponent,
    EditBookComponent,
    ForgetPasswordComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxSpinnerModule,
    LottieModule.forRoot({ player: playerFactory })

  ],
  providers: [NgxImageCompressService,{
    provide: HTTP_INTERCEPTORS,
    useClass: HttpconfigInterceptor,
    multi: true
  },],
  bootstrap: [AppComponent]
})
export class AppModule { }

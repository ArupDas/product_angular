import { Component, OnInit } from '@angular/core';
// require('aws-sdk/dist/aws-sdk');
import * as AWS from 'aws-sdk/global';
import * as S3 from 'aws-sdk/clients/s3';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { MustMatch } from '../_helpers/must-match.validator';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from "ngx-spinner";
import Swal from 'sweetalert2';
import { NgxImageCompressService } from 'ngx-image-compress';




const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Component({
  selector: 'app-bookpost',
  templateUrl: './bookpost.component.html',
  styleUrls: ['./bookpost.component.css']
})
export class BookpostComponent implements OnInit {
  postUserForm: FormGroup;
  submitted = false;
  public imagePath;
  imgURL: any;
  public frontbookimage: any;
  apiEndPoint = environment.apiUrl;
  ImageFile: any = null;

  //compress image variable
  file: any;
  localUrl: any;
  localCompressedURl: any;
  sizeOfOriginalImage: number;
  sizeOFCompressedImage: number;
  imgResultBeforeCompress:string;
imgResultAfterCompress:string;


  constructor(private formBuilder: FormBuilder, private http: HttpClient, private imageCompress: NgxImageCompressService, private router: Router, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    let useridentify = localStorage.getItem('accessToken');
    if (useridentify) {
      this.formInilization();
    }
    else {
      Swal.fire("Please first Login, Then Post a Book");
      this.router.navigateByUrl('login');
    }
  }

  formInilization() {
    this.postUserForm = this.formBuilder.group({
      bookname: ['', [Validators.required]],
     
    }
    )
  }
  get f() { return this.postUserForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.postUserForm.valid) {
      let body = {
        productName:this.postUserForm.value.bookname,
        imgUrl:'dummy url'
      }
      this.http.post(this.apiEndPoint + 'product', body).subscribe((response) => {
        if(response){
          // this.spinner.hide();
          Swal.fire('You Post Your Book Successfully');
          this.router.navigateByUrl('newhome');
        }
       
      },err => {
        // this.spinner.hide();
        console.log("err",err)
        Swal.fire(err.error.message);
      })
    } else {
      Swal.fire("Please fill full and correct information");
    }
  }

  // uploadData(guid) {
  //   let _userid = localStorage.getItem('userid');
  //   this.postUserForm.value.frontbookimage = guid;
  //   this.spinner.show();
  //   this.http.post(this.apiEndPoint + 'postBookDetail/' + _userid, this.postUserForm.value).subscribe((response) => {
  //     this.spinner.hide();
  //     Swal.fire('You Post Your Book Successfully');
  //     this.router.navigateByUrl('newhome');
  //   })
  // }
 
  // uploadImage(guid) {
  //   return new Promise((resolve, reject) => {
  //     // console.log("===imae file in buckut",this.ImageFile);
  //     // let AWSService = (<any>window).AWS;
  //     // AWSService.config.accessKeyId = 'AKIAJIKMW36RDZPWRKWQ';
  //     // AWSService.config.secretAccessKey = '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';
  //     // // AWSService.config.region = 'us-east-1';
  //     // let bucket = new AWSService.S3({ params: { Bucket: 'secondhandbookimage' } });
  //     // let params = { Key: guid, Body: this.ImageFile, ContentType: this.ImageFile.type };
  //     // console.log("============ I am here",bucket);
  //     // bucket.upload(params, function (error, res) {
  //     //     console.log('error', error);
  //     //    console.log('response', res);
  //     //   resolve();
  //     // })


  //     const bucket = new S3(
  //       {
  //           accessKeyId: 'AKIAJIKMW36RDZPWRKWQ',
  //           secretAccessKey: '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',
  //           region: 'ap-south-1'
  //       }
  //   );
  //   const params = {
  //     Bucket: 'secondhandbookimage',
  //     Key: guid,
  //     Body: this.ImageFile,
  //     ContentType: this.ImageFile.type,
  //     ACL: 'public-read'

  // };

  //   bucket.upload(params, function (error, res) {
  //         console.log('error', error);
  //        console.log('response', res);
  //       resolve();
  //     })

  //   });

  // }

  // isbnExample() {
  //   Swal.fire({
  //     title: 'You can find ISBN No. to the back cover of your book or 1st, 2nd page from front side',
  //     imageUrl: 'assets/images/isbnImage.jpg',
  //     imageHeight: 200,
  //     imageWidth: 200

  //   })
  // }
  // createGuid() {
  //   return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
  //     var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
  //     return v.toString(16);
  //   });
  // }

  // previewimage(files, event) {
  //   var  fileName : any;
  //   if (files.length === 0)
  //     return;
  //   const file = event.target.files[0];
  //   this.ImageFile = file;
  //   fileName = this.ImageFile['name'];
  //   console.log("ImageFile ==>>>", this.ImageFile);
  //   var reader = new FileReader();
  //   this.imagePath = files;

  //   reader.readAsDataURL(files[0]);
  //   reader.onload = (_event) => {
  //     this.imgURL = reader.result;
  //     this.localUrl = reader.result;
  //     this.compressFile(this.localUrl,fileName);
  //   }
  //  // reader.readAsDataURL(event.target.files[0]);

  // }

  // compressFile(image,fileName) {
  //   this.ImageFile = null;
  //   var orientation = -1;
  //   this.sizeOfOriginalImage = this.imageCompress.byteCount(image)/(1024*1024);
  //   console.warn('Size in bytes is now:',  this.sizeOfOriginalImage);
  //   this.imageCompress.compressFile(image, orientation, 50, 50).then(
  //   result => {
  //   this.imgResultAfterCompress = result;
  //   this.localCompressedURl = result;
  //   this.sizeOFCompressedImage = this.imageCompress.byteCount(result);
  //   console.warn('Size in bytes after compression:',  this.sizeOFCompressedImage);
  //   // create file from byte
  //   const imageName = fileName;
  //   // call method that creates a blob from dataUri
  //  const imageBlob = this.dataURItoBlob(this.imgResultAfterCompress.split(',')[1]);
  //   //imageFile created below is the new compressed file which can be send to API in form data
  //   this.ImageFile = new File([imageBlob], imageName, { type: 'image/jpeg' });
  //   console.log("===cpmress file",this.ImageFile);
  //   });}

  //   dataURItoBlob(dataURI) {
  //     const byteString = window.atob(dataURI);
  //     const arrayBuffer = new ArrayBuffer(byteString.length);
  //     const int8Array = new Uint8Array(arrayBuffer);
  //     for (let i = 0; i < byteString.length; i++) {
  //     int8Array[i] = byteString.charCodeAt(i);
  //     }
  //     const blob = new Blob([int8Array], { type: 'image/jpeg' });
  //     return blob;
  //     }

}

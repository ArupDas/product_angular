import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuybookpaymentComponent } from './buybookpayment.component';

describe('BuybookpaymentComponent', () => {
  let component: BuybookpaymentComponent;
  let fixture: ComponentFixture<BuybookpaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuybookpaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuybookpaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from "ngx-spinner";
import Swal from 'sweetalert2'

@Component({
  selector: 'app-buybookpayment',
  templateUrl: './buybookpayment.component.html',
  styleUrls: ['./buybookpayment.component.css']
})
export class BuybookpaymentComponent implements OnInit {

  bookdata: any = [];
  paytm = [];
  apiEndPoint = environment.apiUrl;
  imageUrl = environment.imageUrl;
  totalPayment:any;
  gstTax = 2;
  checkRole:any;
  constructor( private http: HttpClient, private router: Router,private spinner: NgxSpinnerService) { }

  ngOnInit() {
    let useridentify = localStorage.getItem('accessToken');
    if (useridentify) {
      let getRoleId = JSON.parse(atob(useridentify.split('.')[1]))
      let permission = getRoleId.permission
      let perms = permission.split(',');
      let obj = {
        create:'',
        update:'',
        fetch:'',
        delete:''
      }
      for(let i = 0; i < perms.length; i++){
        console.log("item",perms[i])
        if(perms[i] == 'create'){
          obj.create  = 'create'
        }
        if(perms[i] == 'update'){
          obj.update  = 'update'
        }
        if(perms[i] == 'fetch'){
          obj.fetch  = 'fetch'
        }
        if(perms[i] == 'delete'){
          obj.delete  = 'delete'
        }
      }

      
      this.checkRole = obj
      let postbookid = localStorage.getItem('postBookId');
      this.http.get(this.apiEndPoint +'product/' + postbookid).subscribe((response: any) => {
        console.log(response)
        this.bookdata = response;
      },err => {
        // this.spinner.hide();
        console.log("err",err)
        Swal.fire(err.error.message);
      })
    }
    else {
      Swal.fire("please first login to buy book");
      this.router.navigateByUrl('login');
    }
  }
  
  delete(productId){
    console.log("pppppp",)
    this.http.delete(this.apiEndPoint +'product/' + productId).subscribe((response: any) => {
      console.log("response",response)
      if(response){
        this.router.navigateByUrl('newhome');
      }
      
    },err => {
      // this.spinner.hide();
      console.log("err",err)
      Swal.fire(err.error.message);
    })
    
  }

  update(val){
    this.router.navigate(['/editbook'],{
      queryParams:{bookId:val}
    });
  }

}

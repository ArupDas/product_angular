import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { MustMatch } from '../_helpers/must-match.validator';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from "ngx-spinner";
import Swal from 'sweetalert2'
import { error } from 'console';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  userForm: FormGroup;
  submitted = false;
  apiEndPoint = environment.apiUrl;
  constructor(private formBuilder: FormBuilder, private http: HttpClient, private router: Router,private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.userForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      role: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      rpassword: ['', [Validators.required, Validators.minLength(8)]],
    }, {
        validator: MustMatch('password', 'rpassword')
      }
    )
  }
  get f() { return this.userForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.userForm.valid) {
      let body = {
        userName:this.userForm.value.username,
        roleId:this.userForm.value.role,
        password:this.userForm.value.password
      }
      console.log("data",body)
      // this.spinner.show();
      this.http.post(this.apiEndPoint +'signup', body).subscribe((response) => {
        // this.spinner.hide();
        console.log("response",response)
        if (response) {
          Swal.fire(response['message']);
        this.router.navigateByUrl('/login');
         
        }
        
      },err => {
        // this.spinner.hide();
        console.log("err",err)
        Swal.fire(err.error.message);
      })
    }
    // else {
    //   alert('Your from is not valid');
    // }
  }

}

import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { catchError } from "rxjs/operators";
import Swal from 'sweetalert2'

@Injectable()
export class HttpconfigInterceptor implements HttpInterceptor {
  clientSecret = ''

  constructor(
    private router: Router,
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (request.url.indexOf('login') > 0
      || request.url.indexOf('signup') > 0
     ) {
        console.log("I am here 1") 
      const secretheader = request.clone({ setHeaders: { client_secret: this.clientSecret, lang: localStorage.getItem('lang') ? '' : 'en' } });
      return next.handle(secretheader);
      // const secretheader = request.clone({ setHeaders: { client_secret: this.clientSecret, lang: localStorage.getItem('lang') ? '' : 'en' } });
      // return next.handle(secretheader);

    }
    else {
        console.log("I am here") 
      // sending incoming request by appending token to header
      return next.handle(request.clone({ setHeaders: { access_token: localStorage.getItem('accessToken'), lang: localStorage.getItem('lang') ? '' : 'en' } }))
        // .catch(error => {

        //     // localStorage.clear();
        //     this.router.navigateByUrl('');
        //     return throwError(error);
          
        // }
        // );
        .pipe(catchError(this.erroHandler))
    }

    

    

  }
  erroHandler(error: HttpErrorResponse) {
    
      if(error){
        console.log("auth error",error.error)
        Swal.fire(error.error.message);
               localStorage.clear();
              this.router.navigateByUrl('');
            return throwError(error);
      }
    // return throwError(error.message || 'server Error');
  }
}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { NgxSpinnerService } from "ngx-spinner";
import Swal from 'sweetalert2'

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  data1: Object;
  id: any;
  submitted = false;
  apiEndPoint = environment.apiUrl;
  constructor(private router: Router, private formBuilder: FormBuilder, private http: HttpClient,private spinner: NgxSpinnerService) { }

  ngOnInit() {
    localStorage.clear();
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(8)]],
    });
  }
  get f() { return this.loginForm.controls; }
  onSubmit() {
    this.submitted = true; 
    if (this.loginForm.valid) {
      let body = {
        userName:this.loginForm.value.username,
        password:this.loginForm.value.password
      }
      // this.spinner.show();
      this.http.post(this.apiEndPoint +'login', body)
        .subscribe((response:any) => {
          // this.spinner.hide();
          if (response) {
            console.log("response",response)
            let accessToken = response.accessToken;
            localStorage.setItem('accessToken', accessToken);
            this.router.navigateByUrl('newhome');
          }
          else {
            Swal.fire(response['message']);
            this.ngOnInit();
          } 
        },err => {
          // this.spinner.hide();
          console.log("err",err)
          Swal.fire(err.error.message);
        }) 
    }
    // else {
    //   alert('invalid Entries');
    // }
  }
}
